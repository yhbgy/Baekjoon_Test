//
//  Test1000.swift
//  Baekjoon_Test
//
//  Created by INSEONG on 2023/07/21.
//

import Foundation


func test1000() {
    let nums = readLine()!.split(separator: " ").map {Int($0)!}
    print("\(nums[0] + nums[1])")
}

func test1001() {
    let nums = readLine()!.split(separator: " ").map {Int($0)!}
    print("\(nums[0] - nums[1])")
}

// 이 문제는 r1, r2를 각각 반지름으로 하는 두 원을 구하고 그 원이 겹치는 좌표가 몇 개인지를 체크하는 문제이다.
// 원의 크기 및 좌표가 완전히 일치할 경우 개수가 무한대인 케이스가 된다. 따라서 (x1 = x2) && (y1 = y2) && (r1 = r2) 일 때 이 케이스이다.
// 두 점 사이의 거리를 구하고, 그 거리가 r1 + r2 보다 작으면 2, 같으면 1, 크면 0이다.
// 백준에서 sqrt안됨.. 왜안됨?ㅜㅜ
func test1002() {
    for _ in 0..<Int(readLine()!)! {
        let nums = readLine()!.split(separator: " ").map {Double($0)!}
        let (x1,y1,r1,x2,y2,r2) = (nums[0], nums[1], nums[2], nums[3], nums[4], nums[5])
        
        // 원이 겹칠 경우 체크
        if x1 == x2 && y1 == y2 && r1 == r2 {
            print(-1)
        }
        
        // 거리계산
        let distanceR:Double = abs(r1 + r2)
        let distanceR2:Double = abs(r1 - r2)
        let distanceX:Double = powl(Double(x1 - x2), 2)
        let distanceY:Double = powl(Double(y1 - y2), 2)
        let distance = sqrt(distanceX + distanceY)
        
        if distanceR2 < distance && distance < distanceR {
            print(2)
        }
        else if distance == distanceR || distance == distanceR2 {
            print(1)
        }
        else {
            print(0)
        }
    }
}

func test1003() {
    func fibonacci(_ n: Int) -> (Int, Int) {
        var dp = Array(repeating: (0, 0), count: n + 1)
        dp[0] = (1, 0)
        dp[1] = (0, 1)

        for i in 2...n {
            dp[i].0 = dp[i - 1].0 + dp[i - 2].0
            dp[i].1 = dp[i - 1].1 + dp[i - 2].1
        }

        return dp[n]
    }
    
    let T = Int(readLine()!)!

    for _ in 0..<T {
        let N = Int(readLine()!)!
        let result = fibonacci(N)
        print("\(result.0) \(result.1)")
    }
}

// 왕자가 행성계안에 포함이 되어있는지 없는지 여부를 따지면 된다.
// 각각의 행성계안에 왕자가 포함이 되어 있으면 count++, 아니면 0
// 단 왕자, 도착점이 같은 행성계안에 있으면 0
func test1004() {
    struct Point {
        var x: Int
        var y: Int
    }

    struct Circle {
        var center: Point
        var radius: Int
    }

    func distance(_ p1: Point, _ p2: Point) -> Double {
        let dx = Double(p1.x - p2.x)
        let dy = Double(p1.y - p2.y)
        return sqrt(dx * dx + dy * dy)
    }

    func isInside(_ p: Point, _ c: Circle) -> Bool {
        let d = distance(p, c.center)
        return d < Double(c.radius)
    }

    func countIntersectingCircles(_ start: Point, _ end: Point, _ planets: [Circle]) -> Int {
        var count = 0
        for planet in planets {
            let startInside = isInside(start, planet)
            let endInside = isInside(end, planet)
            
            if startInside != endInside {
                count += 1
            }
        }
        return count
    }
    
    let T = Int(readLine()!)!
    
    for _ in 0..<T {
        let points = readLine()!.split(separator: " ").map { Int($0)! }
        let start = Point(x: points[0], y: points[1])
        let end = Point(x: points[2], y: points[3])
        
        let n = Int(readLine()!)!
        var planets = [Circle]()
        for _ in 0..<n {
            let planetInfo = readLine()!.split(separator: " ").map { Int($0)! }
            let center = Point(x: planetInfo[0], y: planetInfo[1])
            let radius = planetInfo[2]
            let planet = Circle(center: center, radius: radius)
            planets.append(planet)
        }
        
        let result = countIntersectingCircles(start, end, planets)
        print(result)
    }
}

func test1005() {
    func topologicalSort(_ graph: inout [[Int]], _ inDegree: inout [Int]) -> [Int] {
        var result = [Int]()
        var queue = [Int]()

        for i in 1..<inDegree.count {
            if inDegree[i] == 0 {
                queue.append(i)
            }
        }

        while !queue.isEmpty {
            let node = queue.removeFirst()
            result.append(node)

            for nextNode in graph[node] {
                inDegree[nextNode] -= 1
                if inDegree[nextNode] == 0 {
                    queue.append(nextNode)
                }
            }
        }

        return result
    }

    func solveACMCraft() {
        let t = Int(readLine()!)!
        for _ in 0..<t {
            let nk = readLine()!.split(separator: " ").map { Int($0)! }
            let n = nk[0]
            let k = nk[1]

            var buildTime = [0]  // 0-based indexing
            buildTime += readLine()!.split(separator: " ").map { Int($0)! }

            var graph = [[Int]](repeating: [], count: n + 1)
            var inDegree = [Int](repeating: 0, count: n + 1)

            for _ in 0..<k {
                let xy = readLine()!.split(separator: " ").map { Int($0)! }
                let x = xy[0]
                let y = xy[1]
                graph[x].append(y)
                inDegree[y] += 1
            }

            let w = Int(readLine()!)!

            let sorted = topologicalSort(&graph, &inDegree)

            var dp = [Int](repeating: 0, count: n + 1)

            for node in sorted {
                for nextNode in graph[node] {
                    dp[nextNode] = max(dp[nextNode], dp[node] + buildTime[node])
                }
            }

            print(dp[w] + buildTime[w])
        }
    }
    
    solveACMCraft()
}
